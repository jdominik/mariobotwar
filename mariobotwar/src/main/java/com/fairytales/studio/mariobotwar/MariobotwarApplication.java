package com.fairytales.studio.mariobotwar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MariobotwarApplication {

	public static void main(String[] args) {
		SpringApplication.run(MariobotwarApplication.class, args);
	}

}
