package com.fairytales.studio.mariobotwar.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fairytales.studio.mariobotwar.entity.Player;

public interface GameRepository extends JpaRepository<Player, Integer> {

}
