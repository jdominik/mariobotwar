package com.fairytales.studio.mariobotwar.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fairytales.studio.mariobotwar.entity.GameBoard;
import com.fairytales.studio.mariobotwar.entity.Player;
import com.fairytales.studio.mariobotwar.service.GameService;

@RestController
@RequestMapping("/game")
public class MarioBotWarController {
	
	private GameService gameService;
	
	@GetMapping("/status")
	public String showCurrentGame() {
		
		return "showgame";
	}
	
	@PostMapping("/play")
	public String playGame(@RequestBody Player player) {
		if (player.getId() == 0) {
			boolean gameStarted = initializePlayer();
			if (gameStarted) {
				throw new RuntimeException("Not able to add new player, game in progress!");
			} else {
				gameService.savePlayer(player);	// setting id and securityKey for player
			}
			processTurnOfGame(player);	// waiting till all request from players will be arrived or max 100 seconds
		}
		return "playgame";
	}

	
	
	private void processTurnOfGame(Player player) {
		// TODO Auto-generated method stub
		
	}

	private boolean initializePlayer() {
		// TODO Auto-generated method stub
		return false;
	}
	
	private GameBoard initializeBoard(int nrPlayers, int nrExits, int nrFlags, int type) {
		/*
		 * areaForPlayers 3x3 areaForFlags middle 4x4 areaForExits perimeter 3x3
		 * areaForWalls perimeter 4 - middle 4
		 * 
		 * for (perimeter : ) {
		 * 
		 * }
		 * 
		 * int imin = perimeter postion from a middle imax
		 * 
		 * for (int j = imin; j < imax; j++) for (int j2 = -imin; j2 > -imax; j2--)
		 */
		
		
		return null;
		
	}

}
