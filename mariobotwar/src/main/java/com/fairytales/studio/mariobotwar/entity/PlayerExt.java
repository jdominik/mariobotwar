package com.fairytales.studio.mariobotwar.entity;

public class PlayerExt extends Player {
	
	private GPS location;
	private TStatus status;
	private GameHistory history;
	
	public PlayerExt() {
		super();
	}

	public PlayerExt(GPS location, TAction currAction, TStatus status, GameHistory history) {
		super();
		this.location = location;
		this.status = status;
		this.history = history;
	}

	public GPS getLocation() {
		return location;
	}

	public void setLocation(GPS location) {
		this.location = location;
	}

	public TStatus getStatus() {
		return status;
	}

	public void setStatus(TStatus status) {
		this.status = status;
	}

	public GameHistory getHistory() {
		return history;
	}

	public void setHistory(GameHistory history) {
		this.history = history;
	}
}
