package com.fairytales.studio.mariobotwar.entity;

// jackson compiled class
public class GameBoard {
	
	private char[][] tableBoard;	// char should be fine..

	public GameBoard(char[][] tableBoard) {
		this.tableBoard = tableBoard;
	}

	public char[][] getTableBoard() {
		return tableBoard;
	}

	public void setTableBoard(char[][] tableBoard) {
		this.tableBoard = tableBoard;
	}
	
	
}
