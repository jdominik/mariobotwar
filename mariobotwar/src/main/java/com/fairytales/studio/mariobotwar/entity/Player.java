package com.fairytales.studio.mariobotwar.entity;


// jackson compiled class
public class Player {
	
	private int id;
	private TColor color;
	private TAction currAction;
	private String ip;
	private long securityKey;
	
	public Player() {
		
	}
	
	public Player(int id, TColor color, TAction currAction, String ip, long securityKey) {
		this.id = id;
		this.color = color;
		this.currAction = currAction;
		this.ip = ip;
		this.securityKey = securityKey;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	public TColor getColor() {
		return color;
	}

	public void setColor(TColor color) {
		this.color = color;
	}


	public TAction getCurrAction() {
		return currAction;
	}


	public void setCurrAction(TAction currAction) {
		this.currAction = currAction;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public long getSecurityKey() {
		return securityKey;
	}


	public void setSecurityKey(long securityKey) {
		this.securityKey = securityKey;
	}

}
