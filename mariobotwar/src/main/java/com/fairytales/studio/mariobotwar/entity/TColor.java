package com.fairytales.studio.mariobotwar.entity;

public class TColor {
	private int id;
	private String name;
	private long RGBCode;
	
	public TColor(int id, String name, long rGBCode) {
		this.id = id;
		this.name = name;
		RGBCode = rGBCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getRGBCode() {
		return RGBCode;
	}

	public void setRGBCode(long rGBCode) {
		RGBCode = rGBCode;
	}
	
	
}
