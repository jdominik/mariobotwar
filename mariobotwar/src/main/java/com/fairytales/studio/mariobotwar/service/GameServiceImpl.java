package com.fairytales.studio.mariobotwar.service;

import com.fairytales.studio.mariobotwar.dao.GameRepository;
import com.fairytales.studio.mariobotwar.entity.Player;

public class GameServiceImpl implements GameService {
	
	private GameRepository gameRepository;
	
	public GameServiceImpl(GameRepository repository) {
		gameRepository = repository;
	}
	
	public void savePlayer(Player player) {
		gameRepository.save(player);
	}

}
