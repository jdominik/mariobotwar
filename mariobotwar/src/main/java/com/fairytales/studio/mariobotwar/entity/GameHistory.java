package com.fairytales.studio.mariobotwar.entity;

public class GameHistory {
	private long id;
	private int gameId;
	private int turn;
	private PlayerExt player;
	private GameBoard gameBoardForTurn;
	
	public GameHistory(long id, int gameId, int turn, PlayerExt player, GameBoard gameBoardForTurn) {
		this.id = id;
		this.gameId = gameId;
		this.turn = turn;
		this.player = player;
		this.gameBoardForTurn = gameBoardForTurn;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public int getTurn() {
		return turn;
	}
	public void setTurn(int turn) {
		this.turn = turn;
	}
	public PlayerExt getPlayer() {
		return player;
	}
	public void setPlayer(PlayerExt player) {
		this.player = player;
	}
	public GameBoard getGameBoardForTurn() {
		return gameBoardForTurn;
	}
	public void setGameBoardForTurn(GameBoard gameBoardForTurn) {
		this.gameBoardForTurn = gameBoardForTurn;
	}
	
	
}
