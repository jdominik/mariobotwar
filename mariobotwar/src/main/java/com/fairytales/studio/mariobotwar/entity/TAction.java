package com.fairytales.studio.mariobotwar.entity;

public enum TAction {	
	UP ("W"),
	DOWN ("S"),
	LEFT ("A"),
	RIGHT ("D"),
	SHOT ("Shift-Shift"),
	NO_ACTION ("Esc");
	
	private final String key;
	
	TAction(String key) {
		this.key = key;
	}
}
