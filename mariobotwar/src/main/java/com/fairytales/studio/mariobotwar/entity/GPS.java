package com.fairytales.studio.mariobotwar.entity;

public class GPS {
	
	private int hei;
	private int wid;

	public GPS() {
		
	}

	public GPS(int hei, int wid) {
		this.hei = hei;
		this.wid = wid;
	}

	public int getHei() {
		return hei;
	}

	public void setHei(int hei) {
		this.hei = hei;
	}

	public int getWid() {
		return wid;
	}

	public void setWid(int wid) {
		this.wid = wid;
	}

	
}
